function main_popup(){
  if (!chrome.tabs){
    setTimeout(() => main_popup(), 2000)
    return
  };

  chrome.tabs.query({active: true,  lastFocusedWindow: true}, tabs => {

    const { id: tabId, url } = tabs[0];

    arr = url.split("/");

    if (!arr[2].includes("www.linkedin.com")){
      alert("This extension is only usable in linkedin profiles");
      return;
    }

    code = "";
    chrome.storage.local.get(
      ['linkedinNameClasses', 'linkedinJobClasses'],
      function(items) {
        code = "["+items.linkedinNameClasses+","+items.linkedinJobClasses+"]";

        chrome.tabs.executeScript(tabId, { code }, postLinkedinData);

        //post linkedin data to background
        function postLinkedinData(result) {
          utc_date = new Date().toJSON();
          utc_date_time = utc_date.slice(0,10) + " " + utc_date.slice(11,19);

          chrome.storage.local.get({
            googleSheetId: '',
          }, function(items) {

            google_sheet_id =  items.googleSheetId

            var data = {
              name: result[0][0],
              job: result[0][1],
              date: utc_date_time,
              profileUrl: url,
              google_sheet_id: google_sheet_id,
            };

            chrome.runtime.sendMessage(data, function(response) {
            });        
          });
        }
      });
  });  
}

main_popup();