// Saves options to chrome.storage
function save_options() {
  var linkedinNameClasses = document.getElementById('linkedin_name_classes').value;
  var linkedinJobClasses = document.getElementById('linkedin_job_classes').value;
  var googleSheetAPIKey = document.getElementById('google_sheet_api_key').value;
  var googleSheetId = document.getElementById('google_sheet_id').value;
  
  console.log(googleSheetAPIKey);
  chrome.storage.local.set({
    linkedinNameClasses: linkedinNameClasses,
    linkedinJobClasses: linkedinJobClasses,
    googleSheetAPIKey: googleSheetAPIKey,
    googleSheetId: googleSheetId,
  }, function() {
    // Update status to let user know options were saved.
    var status = document.getElementById('status');
    status.textContent = 'Options saved.';
    setTimeout(function() {
      status.textContent = '';
    }, 750);
  });
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {
  chrome.storage.local.get({
    linkedinNameClasses: '',
    linkedinJobClasses: '',
    googleSheetAPIKey: '',
    googleSheetId: '',
  }, function(items) {
    document.getElementById('linkedin_name_classes').value = items.linkedinNameClasses;
    document.getElementById('linkedin_job_classes').value = items.linkedinJobClasses;
    document.getElementById('google_sheet_api_key').value = items.googleSheetAPIKey;
    document.getElementById('google_sheet_id').value = items.googleSheetId;

  });
}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click',
    save_options);