# get_info_linkedin

Chrome Extension to save the basic info from a linkedin profile into a google sheet.

- Enable your google api (sheet)
- Create your API KEY, and Client ID
- Create an google developer account
- Add your email on the OAUTH consent
- Add your email to the users of the google sheet (or make it public)

(based on https://bumbu.me/gapi-in-chrome-extension)
