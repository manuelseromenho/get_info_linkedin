const DISCOVERY_DOCS = ["https://sheets.googleapis.com/$discovery/rest?version=v4"];
const SPREADSHEET_TAB_NAME = 'Folha1';

// load Google API client
function onGAPILoad() {
  chrome.storage.local.get(
    ['googleSheetAPIKey'],
    function(items) {

      gapi.client.init({
        apiKey: items.googleSheetAPIKey,
        discoveryDocs: DISCOVERY_DOCS,
      });
    });
}

// Listen for messages from popup.js
chrome.extension.onMessage.addListener(
  function(request, sender, sendResponse) {

        // Get the token
        chrome.identity.getAuthToken({interactive: true}, function(token) {
          debugger;

          gapi.auth.setToken({
            'access_token': token,
          });

          const {name, job, date, profileUrl, google_sheet_id} = request;

          const linkedinData = {values: [[
            name,
            job, 
            date,
            profileUrl,
            ]]};

            function writeToSpreadSheets(){
              if (!gapi.client.sheets){
                setTimeout(() => writeToSpreadSheets(), 2000)
                return
              };
              gapi.client.sheets.spreadsheets.values.append({
                spreadsheetId: google_sheet_id,
                range: SPREADSHEET_TAB_NAME,
                valueInputOption: 'USER_ENTERED',
                resource: linkedinData
              }).then((response) => {
                sendResponse({success: true});
              });

            }
            writeToSpreadSheets();
          });
        return true;
      });